/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.android.sunshine.app.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.android.sunshine.app.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from build type: debug
  public static final String OPEN_WEATHER_MAP_API_KEY = "dd3dd0800d02338c3c67a3afadf35ad9";
}
